﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tic_tac_toe
{
    class Program
    {
        static String[] board = new String[9];
        static String playsAgain = "Y";
        static int counter = 0;

        static void initializeVariable()
        {
            for (int i = 0; i < 9; i++)
            {
                board[i] = i.ToString();
            }
        }

        static void playAgainMsg(String message)
        {
            Console.WriteLine(message + "Kom igen daa, en till match?");
            if (Console.ReadLine().Equals("Y"))
                playsAgain.Equals("Y");
            else
                playsAgain.Equals("N");
        }

        static void Main(string[] args)
        {
            introduction();
            while (playsAgain.Equals("Y"))
            {
                initializeVariable();
                while (hasWon() == false && counter < 9)
                {
                    askData("X");
                    if (hasWon() == true)
                        break;
                    askData("O");
                }
                if (hasWon() == true)
                    playAgainMsg("Egd! ");
                else
                    playAgainMsg("Hee, ingen vann!");
            }
        }


        static void askData(String player)
        {
            Console.Clear();

            Console.WriteLine("Player: " + player);
            int selection;

            while (true)
            {
                Console.WriteLine("Välj föfan:\n");
                drawBoard();
                try
                {
                    selection = Convert.ToInt32(Console.ReadLine());
                    if ((board[selection].Equals("X") || board[selection].Equals("O")))
                        Console.WriteLine("Alltså... är du retard?");
                    else
                        break;
                }
                catch(IndexOutOfRangeException)
                {
                    Console.WriteLine("NEEEJ! Det där går inte!");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Det måste vara en siffra");
                }
            }
            board[selection] = player;


        }

        static void drawBoard()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            for (int i = 0; i < 7; i += 3)
                Console.WriteLine(board[i] + "|" + board[i + 1] + "|" + board[i + 2]);
            Console.ResetColor();
        }

        static Boolean hasWon()
        {
            for (int i = 0; i < 7; i += 3)
            {
                if (board[i].Equals(board[i + 1]) && board[i + 1].Equals(board[i + 2]))
                {
                    return true;
                }
            }
            if (board[0].Equals(board[3]) && board[3].Equals(board[6]))
                return true;
            if (board[1].Equals(board[4]) && board[4].Equals(board[7]))
                return true;
            if (board[2].Equals(board[5]) && board[3].Equals(board[8]))
                return true;
            if (board[2].Equals(board[4]) && board[4].Equals(board[6]))
                return true;
            if (board[0].Equals(board[4]) && board[4].Equals(board[8]))
                return true;
            return false;
        }


        static void introduction()
        {
            Console.ForegroundColor = ConsoleColor.Green ;
            Console.Title = ("Tic Tac muddafakka(falsett)!");
            Console.WriteLine("Tjenareee, det här Tic Tac muddafakka(falsett).\n");
            Console.WriteLine("Tryck på tangenterna.\n");
            Console.ResetColor();
            Console.ReadLine();
            Console.Clear();
        }

    }
}
